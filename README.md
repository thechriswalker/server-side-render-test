# JS/Go Server Side Async Rendering with React/Redux/React-Router

An experiment using `duktape` as a JS engine in Go and creating a simple async rendering server using `react-router`.

## info

Uses `webpack` to create the `client` js and `index.html` (which has placeholders for rendered bits). Webpack also creates the `server-render` library as a UMD modules which can then be loaded by `node` or the `duktape` engine.

The async rendering is done by using a higher-order-component `loadable` which uses `Route.onEnter` to perform loading, waiting on the server, but using a loading state on the client. The `onEnter` hooks are added programmatically by the `createRoutes` function when a `loadable` component is found in the route.

Uses  for `duktape-fetch` in the Go server, and a simple (but un-battle-tested) wrapper for fetch in Node.js. Both of these allow shortcutting the network for local fetch requests and using the `http.Handler` / `express` app directly.

The idea, especially the minimal `req`,`res` implementations, comes from https://github.com/bahmutov/express-service

## running

Install deps: `npm install` and `go get gopkg.in/olebedev/go-duktape.v2 gopkg.in/olebedev/go-duktape-fetch.v2`

Build JS: `npm run build`

Then either:
  - Start JS Server: `npm start`
  - Start GO Server: `go run server.go`


