package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strings"
	"sync"
	"time"

	"gopkg.in/olebedev/go-duktape-fetch.v2"
	"gopkg.in/olebedev/go-duktape.v2"
)

func replaceAll(r map[string]string, s string) string {
	str := s
	for oldStr, newStr := range r {
		str = strings.Replace(str, oldStr, newStr, 1)
	}
	return str
}

type RenderResult struct {
	Id              uint8
	Status          int
	Redirect, Error string
	State           template.JS
	Title           template.HTML
	Meta            template.HTML
	HTML            template.HTML
}

func main() {
	indexHtml, err := ioutil.ReadFile("./build/static/index.html")
	if err != nil {
		log.Panic(err)
	}
	indexAsGoTpl := replaceAll(map[string]string{
		"<!-- META -->":       "{{ .Meta }}",
		"<!-- TITLE -->":      "{{ .Title }}",
		"<!-- HTML -->":       "{{ .HTML }}",
		"/* INITIAL-STATE */": `window["--initial-state"]={{ .State }}`,
	}, string(indexHtml))

	indexTemplate, _ := template.New("index").Parse(indexAsGoTpl)
	if err != nil {
		log.Panic(err)
	}

	mux := http.NewServeMux()
	react := NewReactRenderer(indexTemplate, "./build/lib/server-render.js", 4, mux)
	mux.Handle("/api/latency/", apiHandler())
	mux.Handle("/assets/", http.StripPrefix("/assets", http.FileServer(http.Dir("./build/static/assets/"))))
	mux.Handle("/", react)

	log.Fatalln(http.ListenAndServe("0.0.0.0:8080", mux))
}

const (
	MIN_LATENCY = float64(0)
	MAX_LATENCY = float64(10000)
	DEF_LATENCY = float64(100)
)

func apiHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var num, latency float64
		_, err := fmt.Sscanf(r.URL.RequestURI(), "/api/latency/%f", &num)
		if err != nil {
			latency = DEF_LATENCY
		} else {
			latency = math.Min(math.Max(MIN_LATENCY, num), MAX_LATENCY)
		}
		//wait
		log.Println(r.URL.RequestURI(), latency)
		<-time.After(time.Duration(latency * 1e6))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, `{"latency":%f}`, latency)
	})
}

type ReactRenderer struct {
	Template    *template.Template
	Concurrency uint8
	vm          *duktape.Context
	res         map[uint8]chan *RenderResult
	index       uint8
	mtx         sync.Mutex
}

func NewReactRenderer(tpl *template.Template, file string, concurrency uint8, mux http.Handler) http.Handler {
	rr := &ReactRenderer{
		Template:    tpl,
		Concurrency: concurrency,
		vm:          duktape.New(),
		res:         make(map[uint8]chan *RenderResult, concurrency),
	}

	// add timers
	rr.vm.PushTimers()

	//shim the console
	err := rr.vm.PevalString(`var console = {
        log:print,
        warn:print,
        error:print,
        info:print
    }`)
	if err != nil {
		panic(err)
	}
	log.Println("Started duktape VM")
	//add our callback function
	rr.vm.PushGlobalGoFunction("__callback__", func(ctx *duktape.Context) int {
		log.Println("__callback__")
		resultString := ctx.SafeToString(-1)
		log.Println("res:", resultString)

		var result RenderResult
		json.Unmarshal([]byte(resultString), &result)
		log.Println("Returning result:", result.Id)
		rr.res[result.Id] <- &result

		return 0
	})
	log.Println("Added global callback")

	// add local-enabled fetch
	fetch.PushGlobal(rr.vm, mux)
	log.Println("Added fetch polyfill")

	//load the sourcecode
	err = rr.vm.PevalFile(file)
	if err != nil {
		panic(err)
	}

	//clear the stack
	rr.vm.PopN(rr.vm.GetTop())

	log.Println("Evaluated webpacked JS")
	//initialise the channels
	for i := uint8(0); i < concurrency; i++ {
		rr.res[i] = make(chan *RenderResult, 1)
	}

	return rr
}

func (rr *ReactRenderer) NextIndex() uint8 {
	rr.mtx.Lock()
	defer rr.mtx.Unlock()
	next := rr.index
	rr.index++
	if rr.index >= rr.Concurrency {
		rr.index = 0
	}
	return next
}

func (rr *ReactRenderer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	next := rr.NextIndex()
	// we need to marshal to JSON to ensure escaped correctly
	location, _ := json.Marshal(r.RequestURI)
	log.Println("Rendering:", r.RequestURI, "index:", next)
	start := time.Now()
	//ES6 module means prop.default, also the render function returns an object, best to stringify.
	err := rr.vm.PevalString(fmt.Sprintf(`serverRender.default(%d, %s, function(res) {
        __callback__(JSON.stringify(res));
    });`, next, string(location)))
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("Eval OK, waiting for result.")
	//wait on the channel.
	result := <-rr.res[next]
	renderTime := time.Since(start)
	log.Println("Rendered:", r.RequestURI, "in", renderTime, "res", result)
	if len(result.Error) > 0 {
		http.Error(w, result.Error, http.StatusInternalServerError)
	} else if len(result.Redirect) > 0 {
		http.Redirect(w, r, result.Redirect, http.StatusMovedPermanently)
	} else {
		//OK!
		h := w.Header()
		h.Set("X-React-Renderer", "GO")
		h.Set("X-React-Render-Time", fmt.Sprintf("%s", renderTime))
		w.WriteHeader(result.Status)
		rr.Template.Execute(w, result)
	}
}
