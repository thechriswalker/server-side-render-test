const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const appConfig = {
    mountId: "app",
    env: process.env.NODE_ENV === "production" ? "production" : "development"
}

function mapObject(obj, fn) {
    return Object.keys(obj).reduce((acc, key) => {
        acc[key] = fn(obj[key]);
        return acc;
    }, {});
}

module.exports = [{
    //Client config
    entry: {
        client: __dirname + "/src/client.js",
    },
    output: {
        publicPath: "/assets/",
        path: __dirname + "/build/static/assets",
        filename: "[name].[hash].js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                include: __dirname,
                loader: "babel"
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify(appConfig.env)
            },
            CONFIG: mapObject(appConfig, JSON.stringify)
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new HtmlWebpackPlugin(Object.assign({}, appConfig, {
            chunks: ["client"],
            template: "src/index.html",
            filename: "../index.html" //bring this out of the assets folder
        }))
    ]
}, {
    //Server config
    entry: {
        server: __dirname + "/src/server-render.js",
    },
    output: {
        path: __dirname + "/build/lib",
        filename: "server-render.js",
        library: "serverRender",
        libraryTarget: "umd" //build this as a UMD module, most flexible
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                include: __dirname,
                loader: "babel"
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify(appConfig.env)
            },
            CONFIG: mapObject(appConfig, JSON.stringify)
        }),
        new webpack.optimize.OccurrenceOrderPlugin()
    ]
}];
