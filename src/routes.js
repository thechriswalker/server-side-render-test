import Home from "./components/Home";
import Welcome from "./components/Welcome";
import Complex from "./components/Complex";
import ErrorPage from "./components/ErrorPage";
import SubComplex from "./components/SubComplex";

const baseRoutes = [
    {
        path: "/",
        component: Home,
        indexRoute: {
            component: Welcome,
        },
        childRoutes: [
            {
                path: "/welcome",
                component: Welcome,
            },
            {
                path: "/complex",
                component: Complex,
                childRoutes: [{
                    path: ":id",
                    component: SubComplex
                }]
            },
            {
                path: "/*",
                component: ErrorPage
            }
        ]
    }
];

const noop = () => {};

//Create routes based on a given redux store.
export default function createRoutes(dispatch, { waitOnEnter = false } = {}) {
    return addLoadHooks(baseRoutes, dispatch, waitOnEnter);
}

function addLoadHooks(routes, dispatch, waitOnEnter) {
    return routes.map(original => {
        const clone = Object.assign({}, original);
        if (clone.component && clone.component.isLoadable) {
            clone.onEnter = waitOnEnter
                ? (routerState, replace, done) => {
                    clone.component.load(routerState.params, dispatch, done);
                }
                : (routerState) => {
                    clone.component.load(routerState.params, dispatch, noop);
                };
        } else if (clone.components) {
            const onEnter = [];
            clone.components = Object.keys(clone.components).reduce((components, key) => {
                components[key] = clone.components[key];
                if (components[key].isLoadable) {
                    onEnter.push(components[key].load);
                }
                return components;
            }, {});
            if (onEnter.length) {
                clone.onEnter = waitOnEnter
                    ? (routerState, replace, done) => {
                        let remaining = onEnter.length;
                        const doneOne = () => {
                            if (--remaining === 0) {
                                done();
                            }
                        };
                        onEnter.forEach(fn => fn(routerState.params, dispatch, doneOne));
                    }
                    : (routerState) => {
                        onEnter.forEach(fn => fn(routerState.params, dispatch, noop));
                    }
            }
        }
        if (clone.childRoutes) {
            clone.childRoutes = addLoadHooks(clone.childRoutes, dispatch, waitOnEnter);
        }
        return clone;
    });
}
