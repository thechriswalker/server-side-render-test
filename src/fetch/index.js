//NOTE NO ES6 MODULES
// this is so this can be used untranspiled.
const fetch = require("node-fetch");
const parse = require("url").parse;

//create a fetch polyfill that uses the local express app for local calls.
module.exports = function expressFetch(app, { origin = false } = {}) {
    //we will need to set these.
    if (!global.Reponse) {
        global.Response = fetch.Response;
        global.Headers = fetch.Headers;
        global.Request = fetch.Request;
    }
    let normalisedOrigin = false;
    let protocol = "https";
    if (origin) {
        const parsedOrigin = parse(origin);
        protocol = parsedOrigin.protocol || "https";
        const port = parsedOrigin.port || (protocol === "http" ? "80" : "443");
        normalisedOrigin = { protocol, hostname: parsedOrigin.hostname, port };
    }

    //the setting of "fetch" in the global scope is left to the consumer.
    return (url, options) => {
        //if protocol relative
        const parsed = parse(url, false, true);
        //check url is same origin.
        if (isSameOrigin(parsed, normalisedOrigin)) {
            return passToExpress(app, parsed.href, options);
        } else {
            return fetch.call(this, /^\/\//.test(url) ? protocol + url : url, options);
        }
    };
}

function isSameOrigin(url, origin) {
    //if no host assume the same origin
    if (!url.host) {
        return true
    }
    if (origin) {
        // we where given an origin to match.
        const protoMatch = url.protocol && url.protocol === origin.protocol;
        const hostMatch = url.hostname === origin.hostname;
        const port = url.port || ((url.protocol || origin.protocol) === "http" ? "80" : "443");
        const portMatch = portport === origin.port;
        return protoMatch && hostMatch && portMatch;
    }
    return false;
}

function passToExpress(app, href, options = {}) {
    console.log("Using sneaky bypass network direct into express", href);
    return new Promise((resolve, reject) => {
        const body = createBody(options);

        //minimal express req
        const req = {
            url: href,
            method: options.method || "GET",
            body: body,
            headers: options.headers || {},
            unpipe: function () {},
            connection: {
                "remoteAddress": "::1"
            }
        };

        const headers = {};

        const res = {
            statusCode: 200,
            setHeader: function setHeader (name, value) {
                headers[name] = value;
            },
            getHeader: function getHeader (name) {
                return headers[name];
            },
            get: function get (name) {
                return headers[name];
            },
            end: (chunk, encoding) => {
                resolve(new fetch.Response(chunk, {
                    status: res.statusCode,
                    headers: headers
                }));
            }
        }
        app(req, res);
    })
    .catch(err => {
        console.log("express-fetch error!", err.stack);
        throw error;
    })
}

function createBody({ body }) {
    return body;
}
