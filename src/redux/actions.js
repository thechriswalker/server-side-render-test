
export const LOADING = "-loading-";

export function setData(key, result) {
    return { type: LOADING, key, result };
}

export function setLoading(key) {
    return { type: LOADING, key, loading: true };
}

export function setError(key, error) {
    return { type: LOADING, key, error };
}

export function setReset(key) {
    return { type:LOADING, key, reset: true };
}
