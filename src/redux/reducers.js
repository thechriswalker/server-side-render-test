import { LOADING } from "./actions";

export function load(state = {}, {type, key, reset = false, loading = false, result,  error}) {
    if (type === LOADING) {
        return Object.assign({}, state, { [key]: reset ? null : { loading, result, error }});
    }
    return state;
}
