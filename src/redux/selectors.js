import { createSelector } from "reselect";

const dataSelectorCache = {};
export function getData(state, id) {
    if (id in dataSelectorCache === false) {
        dataSelectorCache[id] = createSelector(s => s.load, load => load[id]);
    }
    return dataSelectorCache[id](state);
}
