import React from "react";

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component'
}

export default function loadable(Component) {
    const loader = typeof Component.load === "function" ? Component.load : false

    return class Loadable extends React.Component {
        static displayName = "Loadable(" + getDisplayName(Component) + ")";
        static isLoadable = true;
        static load(...args) {
            if(loader) {
                loader(...args);
            } else {
                args.pop()(); //get last item. call.
            }
        };

        render() {
            return <Component {...this.props} />
        }
    }
}

