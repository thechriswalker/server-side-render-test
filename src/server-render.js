// server side entry point add a global render function.
import React from "react";
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { renderToString } from "react-dom/server";
import { match, RouterContext, } from "react-router";
import { routerReducer } from "react-router-redux";
import createRoutes from "./routes";
import Helmet from "react-helmet";

import * as appReducers from "./redux/reducers";

// we define some things out of the closure.
const reducers = combineReducers({
    ...appReducers,
    routing: routerReducer
});


export default function render(id, location, callback) {

    //the result boils down to this
    const result = {
        id,
        status: 500,
        html: "", // the generates App HTML
        title: "", // the page title
        meta: "", //the page meta tags (as string)
        state: "", //the page initial state (as string)
        error: "", //the error message if one occured
        redirect: "" // the new redirect location.
    }

    const store = createStore(reducers, applyMiddleware(thunk));

    const routes = createRoutes(store.dispatch, { waitOnEnter: true });

    try {
        match({ routes, location }, (error, redirect, renderProps) => {
            try {
                if (error) {
                    result.error = (stack in error) ? error.stack : "" + error;
                } else if(redirect) {
                    result.redirect = redirect.pathname + redirect.search + redirect.hash;
                    result.status = 302
                } else {
                    result.html = renderToString(<Provider store={store}>
                        <RouterContext {...renderProps} />
                    </Provider>);
                    const { title, meta } = Helmet.rewind();
                    //how to spot an error page? and which is it?
                    //maybe render a comment? /* status:403 */
                    result.status = 200;
                    if (renderProps.components) {
                        renderProps.components.forEach(cmp => {
                            if (cmp.name === "ErrorPage") {
                                result.status = 404;
                            }
                        });
                    }
                    result.title = title.toString();
                    result.meta = meta.toString();
                    result.state = JSON.stringify(store.getState());
                }
            } catch(renderError) {
                result.error = renderError.stack;
            }
            return callback(result);
        });
    } catch(matchError) {
        result.error = matchError.stack;
        return callback(result);
    }
}
