// Client Side entry point
import React from "react";
import { render } from "react-dom";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { Router, browserHistory } from "react-router";
import { routerReducer, routerMiddleware, syncHistoryWithStore } from "react-router-redux";
import thunk from "redux-thunk";

import * as appReducers from "./redux/reducers";
import createRoutes from "./routes";

const store = createStore(
    combineReducers({
        ...appReducers,
        routing: routerReducer
    }),
    window["--initial-state"],
    applyMiddleware(
        thunk,
        routerMiddleware(browserHistory)
    )
);

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);

// Create our routes.
const routes = createRoutes(store.dispatch, { waitOnEnter: false });

//render
render(<Provider store={store}>
    <Router routes={routes} history={history} />
</Provider>, document.getElementById(CONFIG.mountId));
