import React from "react";
import Helmet from "react-helmet";

export default class Welcome extends React.Component {
    render() {
        return (
            <div>
                <h2>Server-Render-Pass: Welcome</h2>
                <Helmet title="Welcome" />
                {this.props.children}
            </div>
        );
    }
}
