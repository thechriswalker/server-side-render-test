import React from "react";
import Helmet from "react-helmet";
import { connect } from "react-redux";
import { setData, setLoading, setError, setReset } from "../redux/actions";
import { getData } from "../redux/selectors";
import loadable from "../loadable";

const load = (id, done) => (dispatch, getState) => {
    const data = getData(getState(), id);
    if (!data) {
        dispatch(setLoading(id))
        fetch("/api/latency/" + (Math.random() * 1000))
            .then(res => res.json())
            .then(data => {
                console.log("got data: ", data);
                //load data
                dispatch(setData(id, { stuff: "thing", id }));
            })
            .catch(err => {
                dispatch(setError(id, err.message))
            })
            .then(() => done());
    } else {
        done();
    }
};

class SubComplex extends React.Component {

    static load(routerParams, dispatch, done) {
        const { id } = routerParams;
        dispatch(load(id, done));
    }

    render() {
        return (
            <div>
                <h4>Server-Render-Pass: SubComplex</h4>
                <Helmet title="SubComplex" />
                <p>{`My data are:`}</p>
                <pre><code>{JSON.stringify(this.props.data, null, "  ")}</code></pre>
                <p><button onClick={this.props.reset}>reload data</button></p>
                {this.props.children}
            </div>
        );
    }
}

export default  connect((state, props) => {
    return {
        data: getData(state, props.params.id)
    };
}, (dispatch, props) => {
    return {
        reset: () => {
            dispatch(setReset(props.params.id));
            SubComplex.load(props.params, dispatch, () => {});
        }
    };
})(loadable(SubComplex));
