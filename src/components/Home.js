// The page structure.
import React from "react";
import { Link } from "react-router";
import Helmet from "react-helmet";

export default class Home extends React.Component {
    render() {
        return (
            <div>
                <h1>Server-Render-Pass</h1>
                <Helmet titleTemplate="%s | Render Test" defaultTitle="Server-Render-Pass" />
                <ul>
                    <li><Link to="/" activeStyle={{color:"red"}} onlyActiveOnIndex>/</Link></li>
                    <li><Link to="/welcome" activeStyle={{color:"red"}}>/welcome</Link></li>
                    <li><Link to="/askdjfa sjdbfkasd jnas" activeStyle={{color:"red"}}>error?</Link></li>
                    <li><Link to="/complex/foo" activeStyle={{color:"red"}}>/complex/foo</Link></li>
                    <li><Link to="/complex/bar" activeStyle={{color:"red"}}>/complex/bar</Link></li>
                </ul>
                {this.props.children}
            </div>
        );
    }
}
