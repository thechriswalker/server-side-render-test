import React from "react";
import Helmet from "react-helmet";
//import SubComplex  from "./SubComplex";

export default class Complex extends React.Component {
    render() {
        return (
            <div>
                <h2>Server-Render-Pass: Complex</h2>
                <Helmet title="Home" />
                {this.props.children}
            </div>
        );
    }
}
