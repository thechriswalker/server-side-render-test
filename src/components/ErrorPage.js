import React from "react";
import Helmet from "react-helmet";

export default class ErrorPage extends React.Component {
    render() {
        return (
            <div>
                <h2>Server-Render-Pass: Error</h2>
                <Helmet title="Error" />
                {this.props.children}
            </div>
        );
    }
}
