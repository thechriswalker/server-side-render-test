const express = require("express");
const prettyHrtime = require("pretty-hrtime");

const render = require("./build/lib/server-render").default;
const fs = require("fs");

const expressFetch = require("./src/fetch");

const indexTemplate = fs.readFileSync(__dirname + "/build/static/index.html", "utf8");

const reduce = (obj, fn, initial) => Object.keys(obj).reduce((acc, key) => fn(acc, key, obj[key]), initial);

const replace = (string, replacements) => reduce(replacements, (str, key, value) => str.replace(key, value), string);

const renderTemplate = data => replace(indexTemplate, {
    "<!-- META -->": data.meta,
    "<!-- HTML -->": data.html,
    "<!-- TITLE -->": data.title,
    "/* INITIAL-STATE */": `window["--initial-state"] = ${data.state || "{}"}`
});

const app = express();

//patch the global fetch to use our app for same origin requests.
global.fetch = expressFetch(app);

function clamp(given, min, max, def) {
    const num = Number(given);
    if (isNaN(num) || !isFinite(num)) {
        return def;
    }
    return Math.min(Math.max(given, min), max);
}

//simple function that introduces latency
app.get("/api/latency/:ms", (req, res) => {
    const time = clamp(req.params.ms, 0, 5000, 100);
    setTimeout(() => {
        res.set("Content-Type", "application/json")
        res.status(200)
        res.end(JSON.stringify({
            latency: time
        }));
    }, time);
})

app.use(express.static(__dirname + "/build/static", { index: false }));

app.use((req, res) => {
    const start = process.hrtime()
    render(null, req.url, data => {
        if(data.error) {
            res.status(data.status).end(data.error);
        } else if (data.redirect) {
            res.redirect(data.status, data.redirect);
        } else {
            const renderTime = prettyHrtime(process.hrtime(start), { precise: true });
            res.set("X-React-Renderer", "JS");
            res.set("X-React-Render-Time", renderTime);
            res.status(data.status).end(renderTemplate(data));
        }
    });
})

app.listen(8080, "0.0.0.0", () => console.log("Now serving on: http://0.0.0.0:8080/"));
